const express = require('express');
const cors = require('cors');
const app = express();
const {join} = require('path');
const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

app.use(express.static(join(__dirname,'client')));
app.use('/form',express.static(join(__dirname,'client','forms')))
app.use('/pdfs',express.static(join(__dirname,'client','pdfs')))

app.listen(PORT,()=>console.log('Server started'))