var sliderSelector = '.s1',
options = {
    init: false,
    // loop: true,
    speed: 800,
    spaceBetween: 30,
      centeredSlides: true,
      // autoplay: {
      //   delay: 1500,
      //   disableOnInteraction: true,
      // },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    effect: 'cube', // 'cube', 'fade', 'coverflow',
    // grabCursor: true,
    // Events
    on: {
        imagesReady: function () {
            this.el.classList.remove('loading');
        }
    }
};
var mySwiper = new Swiper(sliderSelector, options);

// Initialize slider
mySwiper.init();
let slide = 0
mySwiper.on('slideChange', function () {
  slide=mySwiper.activeIndex
});

document.querySelectorAll('.container').forEach(element => {
  element.addEventListener('click',()=>{
    sessionStorage.setItem("item",element.id)
    window.location.assign(`${window.location.protocol}//${window.location.host}/form`)
  })
});